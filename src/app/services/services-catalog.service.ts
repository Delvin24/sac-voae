import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ServicesCatalogService {

  constructor(private http: HttpClient) { }

  getSpecialitiesServices(){
    return this.http.get(environment.baseUrl + '/general/getSpecialitiesServices');
  }

  getSpecialities(){
    return this.http.get(environment.baseUrl + '/general/specialitiesConfig');
  }

}
