import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: "app-new-pass",
  templateUrl: "./new-pass.component.html",
  styleUrls: ["./new-pass.component.scss"],
})
export class NewPassComponent implements OnInit {
  focus;
  focus1;

  showPass: boolean = false;
  constructor(private authService: AuthService) {}

  ngOnInit() {}

  changePass = new FormGroup(
    {
      password: new FormControl("", [
        Validators.required,
        Validators.pattern("^[0-9A-Za-zd$@$!%*?&-_].{7,}"),
      ]),
      tempPass: new FormControl("", [Validators.required]),
    }
  );

  // passNotMatch(Form: FormGroup) {
  //   let pass = Form.get("password").value;
  //   let passConfirm = Form.get("tempPass").value;
  //   return pass == passConfirm ? null : { passNotMatch: true };
  // }

  changePassword() {
       
    if(this.changePass.valid) {
      this.authService.changePassword(this.changePass.value);
    }
  }
}
