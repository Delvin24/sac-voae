import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service'
@Component({
  selector: 'app-recovery-pass',
  templateUrl: './recovery-pass.component.html',
  styleUrls: ['./recovery-pass.component.scss']
})
export class RecoveryPassComponent implements OnInit {
  focus1;
  focus2;
  constructor(
    private authService:AuthService
  ) { }

  ngOnInit(): void {
  }

  recoveryForm = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$"),
    ]),
    dni: new FormControl('', [
      Validators.required,
      Validators.pattern('[0-9]{13}'),
    ]),
  });

  cambiarContrasenia(){
      console.log(this.recoveryForm.value);



      this.authService.recoveryPassword(this.recoveryForm.value);

  }

}
