import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ServicesCatalogService } from './../../services/services-catalog.service';

@Component({
  selector: 'app-quote-services',
  templateUrl: './quote-services.component.html',
  styleUrls: ['./quote-services.component.scss'],
})
export class QuoteServicesComponent implements OnInit {
  constructor(private ServicesCatalogService: ServicesCatalogService) {}

  ngOnInit(): void {
    this.getAttentionTypes();
    this.getServices();
  }

  attentionTypes: any[] = [];
  initialServices: any[] = [];
  services: any[] = [];
  chosenServices: any[] = [];
  serviceFilter: string = '';

  get totalAmount() {
    if (this.chosenServices.length > 0) {
      let min: number = this.chosenServices.reduce(function (a, b) {
        return a + b.min_price;
      }, 0);
      let max: number = this.chosenServices.reduce(function (a, b) {
        return a + b.max_price;
      }, 0);
      return { min, max };
    }
    return null;
  }

  changeServices() {}

  getAttentionTypes() {
    this.ServicesCatalogService.getSpecialities().subscribe((res: any) => {
      this.attentionTypes = res.specialities;
    });
  }

  getServices() {
    this.ServicesCatalogService.getSpecialitiesServices().subscribe(
      (res: any) => {
        this.services = res;
        this.initialServices = res;
      }
    );
  }

  chooseService(service: any) {
    this.chosenServices.push(service);
    this.services = this.services.filter((el) => {
      return el.rel_id != service.rel_id;
    });
  }

  discardService(service: any) {
    this.services.push(service);
    this.chosenServices = this.chosenServices.filter((el) => {
      return el.rel_id != service.rel_id;
    });
  }
}
