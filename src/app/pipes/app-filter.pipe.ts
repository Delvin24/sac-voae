import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'appFilter',
})
export class AppFilterPipe implements PipeTransform {
  transform(items: any, searchText: string): any[] {
    if (!items) {
      return [];
    }
    if (!searchText) {
      return items;
    }
    searchText = searchText.toLocaleLowerCase();
    try {
      return items.filter((element) => {
        if (typeof element === 'object') {
          return Object.keys(element).some((key) => {
            return (
              typeof element[key] === 'string' &&
              element[key].toLocaleLowerCase().includes(searchText)
            );
          });
        } else {
          return element.toLocaleLowerCase().includes(searchText);
        }
      });
    } catch (error) {
      return null;
    }
  }
}
