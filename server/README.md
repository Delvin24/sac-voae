# Configuraciones necesarias

## 1. Instalar dependencias
> composer install

## 2. Cargar clases
> composer dump-autoload

## 3. Crear archivo variables de entoro .env 
Copiar el contenido que hay en .env.example y pegarlo en un nuevo archivo con el 
nombre .env

## 4. Ejecutar migraciones
> php artisan migrate:fresh --seed

# Ejecutar el servicio
> php artisan serve

Si lo desean pueden configurar un host virtual para no tener que levantar el servicio.

# Estructura del proyecto (aspectos mas importantes)
- La carpeta vendor es similar a node_modules.
- app/Entities: Se encuentran los modelos de la base de datos.
- app/Http/Controllers: Controladores para resolver las peticiones.
- routes/api.php: En dicho archivo se colocaran todas las rutas del API.
- database: En este directorio se colocan las migraciones y los seeders.

## Flujo de trabajo (resumido)
1. Primero se creara una ruta en el archivo api.php, se debera usar el metodo 
   correspondiente: GET, POST (PUT, DELETE) y a su vez se debera considerar si
   la ruta necesita autenticacion por JWT para lo cual se debe usar el middleware
   auth:api. 
   Ejemplo de una ruta Route::metodoHTTP('url', 'claseControllador@metodo');
                       Route::GET('users/', 'UsersController@getUsers');
2. Se crea el metodo especificado en la ruta y posteriormente se agrega la logica
   necesaria del mismo.

3. Los metodos reciben data con una instancia de la clase Request.
4. La respuesta se hace con el helper response()->json() de laravel.

### Fin




