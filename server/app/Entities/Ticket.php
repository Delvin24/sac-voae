<?php

namespace App\Entities;


use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = 'tickets';
    public $timestamps = true;
    // public $timestamps = false;

    protected $fillable = [
        'code',
        'clinic_id',
        'status_id',
        'patient_id',
        'attention_type_id',
        'fecha',
        'attended_by',
        'is_new',
        'emergency',
        'type_pacent_id',
    ];
}
