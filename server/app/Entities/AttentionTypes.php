<?php

namespace App\Entities;


use Illuminate\Database\Eloquent\Model;

class AttentionTypes extends Model
{
    protected $table = 'attention_types';
    public $timestamps = false;

    protected $fillable = [
        'attention_type_id',
        'attention_type_name',
        'attention_state',
    ];
}
