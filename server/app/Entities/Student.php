<?php

namespace App\Entities;


use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'students';
    
    // public $timestamps = false;

    protected $fillable = [
        'name',
        'account_number',
        'email',
        'password',
        'temporal_password',
        'created_at',
        'updated_at'
    ];
}
