<?php

namespace App\Entities;


use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $table = 'roles';
    public $timestamps = false;

    protected $fillable = [
        'role_id',
        'role_name'
    ];
}


